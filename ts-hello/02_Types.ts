// Types
let a: number;
let b: boolean;
let c: string;
let d: any;
let e: number[] = [1, 2, 3, 4, 5];
let f: any[] = [1, true, 'a', {}, [], false];

const colorRed = 0;
const colorGreen = 1;
const colorBlue = 2;

enum Color { Red = 0, Green = 1, Blue = 2 };
let backgroundColor = Color.Red;
console.log(backgroundColor);