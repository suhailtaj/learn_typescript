
interface Point {
    x: number,
    y: number
}


// Way - 1 : inline annitation
let drawPoint1 = (point: { x: number, y: number }) => {
    //....
}

// Way - 2 : Interface - Pascal Naming Convension
let drawPoint = (point: Point) => {
    //....
}



drawPoint({
    x: 1,
    y: 2
})