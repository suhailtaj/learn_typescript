class Point {
    constructor(private _x?: number, private _y?: number) {
        this._x = _x;
        this._y = _y;
    }
    draw() {
        console.log('A: ' + this._x + ', B: ' + this._y);
    }

    // Read access to _x
    get x(): number {
        return this._x;
    }

    // Write access to _x
    set x(value) {
        if (value < 0) {
            throw new Error('Value should be greater than 0.');
        }
        this._x = value;
    }

    // Read access to _y
    get y(): number {
        return this._y;
    }

    // Write access to _y
    set y(value) {
        if (value < 0) {
            throw new Error('Value should be greater than 0.');
        }
        this._y = value;
    }
}
let point = new Point(1, 4);

let x = point.x;
let y = point.x;
console.log(x, y);
point.x = 20;
point.draw();

