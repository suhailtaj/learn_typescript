
// Type Assertions - intelence only
let msg = 'abc'; //type of variable is string
let endsWithc = msg.endsWith('c');

let msg1;
msg1 = 'abc'; // type of variable is any

let msg1Assertion = (<string>msg1).endsWith('c');
let AlternaviveWay = (msg1 as string).endsWith('c');