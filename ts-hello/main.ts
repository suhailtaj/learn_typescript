let passcode:string = "secret passcode";

class Employee {
    private _fullName: string;
    private _passcode: string;
    get fullName(): string {
        return this._fullName;
    }

    set fullName(newName: string) {
        if (passcode && passcode == "secret passcode") {
            this._fullName = newName;
        }
        else {
            console.log("Error: Unauthorized update of employee!");
        }
    }

    set passcode (value){

        this._passcode = value;
    }
}

let employee = new Employee();
employee.fullName = "Suhail Taj";
if (employee.fullName) {
    console.log(employee.fullName);
}