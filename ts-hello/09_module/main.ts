// To run this file : 
// tsc --target ES5 09_module/main.ts && node 09_module/main.js
import { Point } from './point';

let point = new Point(1, 4);

let x = point.x;
let y = point.y;
console.log(x, y);
point.x = 20;
point.draw();
